#include QMK_KEYBOARD_H
#include "keymap_uk.h" // UK keymap
#include "sendstring_uk.h"  // UK sendstring map

enum ut472_layers { // Define layers
  _WORKMAN,
  _QWERTY,
  _NUM,
  _FUN,
  _TAB,
  _ADJUST,
};

enum ut472_keycodes {
  WORKMAN = SAFE_RANGE,
  QWERTY,
  XL_PS, // Excel Paste Special
  XL_DR, // Excel delete row
  XL_DC, // Excel delete column
  XL_IR, // Excel insert row
  XL_IC, // Excel insert column
  PASTE_EMAIL,  // Paste my email address
  TOG_CONST_BLGHT, // Toggle backlight on base layer
};

enum tapdance { // Tap dance enums
    TD_QUOTE,
    TD_COM_LBR,
    TD_DOT_RBR,
    TD_SLS_BSLS,
    TD_SCOL_COL,
};

qk_tap_dance_action_t tap_dance_actions[] = {
    [TD_QUOTE] = ACTION_TAP_DANCE_DOUBLE(UK_QUOT, UK_DQUO),
    [TD_SLS_BSLS] = ACTION_TAP_DANCE_DOUBLE(UK_SLSH, UK_BSLS),
    [TD_COM_LBR] = ACTION_TAP_DANCE_DOUBLE(UK_COMM, UK_LPRN),
    [TD_DOT_RBR] = ACTION_TAP_DANCE_DOUBLE(UK_DOT, UK_RPRN),
    [TD_SCOL_COL] = ACTION_TAP_DANCE_DOUBLE(UK_SCLN, UK_COLN),
};

// Custom key combos
#define LT4_TAB LT(4, KC_TAB) // Tap Tab for Tab, hold Tab for layer 4
#define NUM TT(_NUM) // TT - Activated until deactivation if tapped, or hold to active temporarily
#define FUN TT(_FUN)
#define XL_CA MEH(KC_F9) // Excel calculate all

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

  /* Base layer - Workman
   * ,----------------------------------------------------------------------------.
   * |   Del  |  Q  |  D  |  R  |  W  |  B  |  J  |  F  |  U  |  P  | '/" |Bspace |
   * |----------------------------------------------------------------------------+
   * |  Tab/L4 |  A  |  S  |  H  |  T  |  G  |  Y  |  N  |  E  |  O  |  I  | En/Sh|
   * |----------------------------------------------------------------------------+
   * |  =/Shift |  Z  |  X  |  M  |  C  |  V  |  K  |  L  | ,/( | ./) | //\ | ;/: |
   * |----------------------------------------------------------------------------+
   * | Esc/Ctl| Gui | Alt | App |  NUM |   Space   |  FUN | Left| Down|  Up |Right|
   * `----------------------------------------------------------------------------'
   */
[_WORKMAN] = LAYOUT(
  KC_DEL,         UK_Q,    UK_D,    UK_R,   UK_W, UK_B,    UK_J, UK_F, UK_U,           UK_P,           TD(TD_QUOTE),    KC_BSPC,
  LT4_TAB,        UK_A,    UK_S,    UK_H,   UK_T, UK_G,    UK_Y, UK_N, UK_E,           UK_O,           UK_I,            RSFT_T(KC_ENT),
  LSFT_T(UK_EQL), UK_Z,    UK_X,    UK_M,   UK_C, UK_V,    UK_K, UK_L, TD(TD_COM_LBR), TD(TD_DOT_RBR), TD(TD_SLS_BSLS), TD(TD_SCOL_COL),
  LCTL_T(KC_ESC), KC_LGUI, KC_LALT, KC_APP, NUM,     KC_SPC,     FUN,  KC_LEFT,        KC_DOWN,        KC_UP,           KC_RGHT
),

  /* QWERTY
   * ,-------------------------------------------------------------------------.
   * | Del |  Q  |  W  |  E  |  R  |  T  |  Y  |  U  |  I  |  O  |  P  |Bspace |
   * |-------------------------------------------------------------------------+
   * |Tab/L4|  A  |  S  |  D  |  F  |  G  |  H  |  J  |  K  |  L  | '/" | En/Sh|
   * |-------------------------------------------------------------------------+
   * |  =/Sh |  Z  |  X  |  C  |  V  |  B  |  N  |  M  | ,/( | ./) | // | ;/: |
   * |-------------------------------------------------------------------------+
   * |Esc/Ctl| Gui | Alt | App |  NUM |  Space  | FUN  | Left| Down|  Up |Right|
   * `-------------------------------------------------------------------------'
   */
[_QWERTY] = LAYOUT(
  KC_DEL,         UK_Q,    UK_W,    UK_E,   UK_R, UK_T, UK_Y, UK_U, UK_I,           UK_O,           UK_P,            KC_BSPC,
  LT4_TAB,        UK_A,    UK_S,    UK_D,   UK_F, UK_G, UK_H, UK_J, UK_K,           UK_L,           TD(TD_SCOL_COL), RSFT_T(KC_ENT),
  LSFT_T(UK_EQL), UK_Z,    UK_X,    UK_C,   UK_V, UK_B, UK_N, UK_M, TD(TD_COM_LBR), TD(TD_DOT_RBR), UK_SLSH,         TD(TD_QUOTE),
  LCTL_T(KC_ESC), KC_LGUI, KC_LALT, KC_APP, NUM,    KC_SPC,   FUN,  KC_LEFT,        KC_DOWN,        KC_UP,           KC_RGHT
),

  /* Layer 2 - NUMBERS / Excel
   * ,-------------------------------------------------------------------------.
   * |     |  7  |  8  |  9  |  £  |     |     |     |     |     |     |       |
   * |-------------------------------------------------------------------------+
   * |   /  |  4  |  5  |  6  |  *  |     |     | Left|Down | Up  |Right|      |
   * |-------------------------------------------------------------------------+
   * |   -   |  1  |  2  |  3  |  +  |XL_PS|     |XL_CA|     |     |     |     |
   * |-------------------------------------------------------------------------+
   * |     |  =  |  0  |  .  |      |          |       |XL_DC|XL_DR|XL_IR|XL_IC|
   * `-------------------------------------------------------------------------'
   */
[_NUM] = LAYOUT(
  _______,     UK_7,   UK_8, UK_9,   UK_PND,         _______, _______, _______, _______, _______, _______, _______,
  KC_KP_SLASH, UK_4,   UK_5, UK_6,   KC_KP_ASTERISK, _______, _______, KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT, _______,
  KC_KP_MINUS, UK_1,   UK_2, UK_3,   KC_KP_PLUS,     XL_PS,   _______, XL_CA,   _______, _______, _______, _______,
  _______,     UK_EQL, UK_0, UK_DOT, _______,            _______,      _______, XL_DC,   XL_DR,   XL_IR,   XL_IC
),

  /* Layer 3 - FUNCTION
   * ,-------------------------------------------------------------------------.
   * |  ~  |  !  |  @  |  #  |  $  |  %  |  ^  |  &  |  *  |  (  |  )  |      |
   * |-------------------------------------------------------------------------+
   * |  `   | F11 | F12 |  {  |  }  |  ¬  |  _  |  -  |  +  |  [  |  ]  |  |   |
   * |-------------------------------------------------------------------------+
   * |       | F1  | F2  | F3  | F4  | F5  | F6  | F7  | F8  | F9  | F10 |     |
   * |-------------------------------------------------------------------------+
   * |     |     |     |CapLk|      |          |       | Vol-| Br- | Br+ | Vol+|
   * `-------------------------------------------------------------------------'
   */
[_FUN] = LAYOUT(
  UK_TILD, UK_EXLM, UK_AT,   UK_HASH, UK_DLR,  UK_PERC, UK_CIRC, UK_AMPR, UK_ASTR, UK_LPRN, UK_RPRN, _______,
  UK_GRV, KC_F11,  KC_F12,  UK_LCBR, UK_RCBR,  UK_NOT,  UK_UNDS, UK_MINS, UK_PLUS, UK_LBRC, UK_RBRC, UK_PIPE,
  _______, KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_MUTE,
  _______, _______, _______, KC_CAPS, _______,     _______,      _______, KC_VOLD, KC_BRID, KC_BRIU, KC_VOLU
),

  /* Layer 4 - Hold Tab
   * ,-------------------------------------------------------------------------.
   * |     |PrScr|     |     |     |     |     |  F2 |  F4 |  F5 |  F9 |  F11  |
   * |-------------------------------------------------------------------------+
   * |      |     |     |     |     |     |     | Left| Down|  Up |Right|      |
   * |-------------------------------------------------------------------------+
   * |       |     |     |PstEm|     |     |     | Home| PgDn| PgUp| End |     |
   * |-------------------------------------------------------------------------+
   * |     |     |     |     |      |          |       |CTRLZ|CTRLY|CTRLC|CTRLV|
   * `-------------------------------------------------------------------------'
   */
[_TAB] = LAYOUT(
  _______, KC_PSCR, _______, _______,     _______, _______, _______, KC_F2,   KC_F4,      KC_F5,      KC_F9,      KC_F11,
  _______, _______, _______, _______,     _______, _______, _______, KC_LEFT, KC_DOWN,    KC_UP,      KC_RGHT,    _______,
  _______, _______, _______, PASTE_EMAIL, _______, _______, _______, KC_HOME, KC_PGDN,    KC_PGUP,    KC_END,     _______,
  _______, _______, _______, _______,     _______,     _______,      _______, LCTL(UK_Z), LCTL(UK_Y), LCTL(UK_C), LCTL(UK_V)
),

  /* Adjust (L1 + L2)
   *                   v-----------------------RGB CONTROL---------------------v
   * ,--------------------------------------------------------------------------
   * |Reset|     |TOGBL| RGB |RGBMO|HUE+ |HUE- |SAT+ |SAT- |BRT+ |BRT- |WORKMAN|
   * |-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-------|
   * |      |     |     |     |     |     |     |     |     |     |     |QWERTY|
   * |------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+------|
   * |       |     |     |     |     |     |     |     |     |     |     |     |
   * |-------+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----|
   * | Dbg |     |     |     |      |           |      |     |     |     |     |
   * `-------------------------------------------------------------------------'
   */
[_ADJUST] = LAYOUT(
  RESET,   _______, TOG_CONST_BLGHT, RGB_TOG, RGB_MOD, RGB_HUI, RGB_HUD, RGB_SAI, RGB_SAD, RGB_VAI, RGB_VAD, WORKMAN,
  _______, _______, _______,         _______, _______, _______, _______,  _______, _______, _______, _______, QWERTY,
  _______, _______, _______,         _______, _______, _______, _______, _______, _______, _______, _______, _______,
  DEBUG,   _______, _______,         _______, _______,     _______,      _______, _______, _______, _______, _______
),
};

// Key Override
const key_override_t shift_bsp_key_override = ko_make_basic(MOD_MASK_SHIFT, KC_BSPACE, KC_DELETE);
const key_override_t shift_quot_key_override = ko_make_basic(MOD_MASK_SHIFT, TD(TD_QUOTE), UK_DQUO);  // Hack to match US keycaps on UK layout
const key_override_t ctrl_q_key_override = ko_make_basic(MOD_MASK_CTRL, UK_Q, LALT(KC_F4));  // Map Ctrl+Q Alt+F4
const key_override_t **key_overrides = (const key_override_t *[]){
    &shift_bsp_key_override,
    &shift_quot_key_override,
    &ctrl_q_key_override,
    NULL // Null terminate the array of overrides
};

/********************************************************************************/
/* Using layers to do RGB underlighting */
/********************************************************************************/

const rgblight_segment_t PROGMEM base_no_bl_layer1[] = RGBLIGHT_LAYER_SEGMENTS({0, 8, HSV_OFF});
const rgblight_segment_t PROGMEM base_no_bl_layer2[] = RGBLIGHT_LAYER_SEGMENTS({0, 8, HSV_OFF});
const rgblight_segment_t PROGMEM base_bl_layer1[] = RGBLIGHT_LAYER_SEGMENTS({0, 8, HSV_WHITE});
const rgblight_segment_t PROGMEM base_bl_layer2[] = RGBLIGHT_LAYER_SEGMENTS({0, 8, HSV_WHITE});
const rgblight_segment_t PROGMEM numbers_layer[] = RGBLIGHT_LAYER_SEGMENTS({0, 8, HSV_GREEN});
const rgblight_segment_t PROGMEM function_layer[] = RGBLIGHT_LAYER_SEGMENTS({0, 8, HSV_CYAN});
const rgblight_segment_t PROGMEM tab_layer[] = RGBLIGHT_LAYER_SEGMENTS({0, 8, HSV_PURPLE});
const rgblight_segment_t PROGMEM adjust_layer[] = RGBLIGHT_LAYER_SEGMENTS({0, 8, HSV_RED});

const rgblight_segment_t* const PROGMEM rgb_layers[] = RGBLIGHT_LAYERS_LIST(base_no_bl_layer1, base_no_bl_layer1, base_bl_layer1, base_bl_layer2, numbers_layer, function_layer, tab_layer, adjust_layer);

// whether backlight should be enabled on base layer
bool const_blight = false;

void keyboard_post_init_user(void) {
  rgblight_sethsv_noeeprom(HSV_OFF);
  // Enable the LED layers
  rgblight_layers = rgb_layers;
}

layer_state_t layer_state_set_user(layer_state_t state) {
  // Access ADJUST layer by holding NUM+FUN
  state = update_tri_layer_state(state, _NUM, _FUN, _ADJUST);

  if (const_blight) {
    rgblight_set_layer_state(2, layer_state_cmp(state, _WORKMAN));
    rgblight_set_layer_state(3, layer_state_cmp(state, _QWERTY));
  } else {
    rgblight_set_layer_state(0, layer_state_cmp(state, _WORKMAN));
    rgblight_set_layer_state(1, layer_state_cmp(state, _QWERTY));
  }
  rgblight_set_layer_state(4, layer_state_cmp(state, _NUM));
  rgblight_set_layer_state(5, layer_state_cmp(state, _FUN));
  rgblight_set_layer_state(6, layer_state_cmp(state, _TAB));
  rgblight_set_layer_state(7, layer_state_cmp(state, _ADJUST));
  return state;
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case QWERTY:
      if (record->event.pressed) {
        set_single_persistent_default_layer(_QWERTY);
      }
      return false;
      break;
    case WORKMAN:
      if (record->event.pressed) {
        set_single_persistent_default_layer(_WORKMAN);
      }
      return false;
      break;
    case PASTE_EMAIL:
      if (record->event.pressed) {
        SEND_STRING("MY EMAIL");
      }
      return false;
      break;
    case XL_PS:
      if (record->event.pressed) {
        register_code(KC_LALT);
        tap_code(KC_E);
        tap_code(KC_S);
        unregister_code(KC_LALT);
      }
      return false;
      break;
    case XL_DR:
      if (record->event.pressed) {
        SEND_STRING(SS_LSFT(" "));
        SEND_STRING(SS_LCTL("-"));
      }
      return false;
      break;
    case XL_DC:
      if (record->event.pressed) {
        SEND_STRING(SS_LCTL(" "));
        SEND_STRING(SS_LCTL("-"));
      }
      return false;
      break;
    case XL_IR:
      if (record->event.pressed) {
        SEND_STRING(SS_LSFT(" "));
        SEND_STRING(SS_LCTL("+"));
      }
      return false;
      break;
    case XL_IC:
      if (record->event.pressed) {
        SEND_STRING(SS_LCTL(" "));
        SEND_STRING(SS_LCTL("+"));
      }
      return false;
      break;
    case TOG_CONST_BLGHT:
      if (record->event.pressed) {
        const_blight = !const_blight;  // https://pastebin.com/gH8XgEfv
      }
      return false;
      break;
  }
  return true;
}
