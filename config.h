#pragma once
#include "config_common.h"
#define TAPPING_TERM 175
#define HOLD_ON_OTHER_KEY_PRESS
#define TAPPING_TOGGLE 2
#define RGBLIGHT_LAYERS
#define RGBLIGHT_MAX_LAYERS 8
#define NO_ACTION_ONESHOT
#define NO_ACTION_MACRO
#define NO_ACTION_FUNCTION